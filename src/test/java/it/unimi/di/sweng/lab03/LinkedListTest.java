package it.unimi.di.sweng.lab03;

import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class LinkedListTest {

	@Rule
    public Timeout globalTimeout = Timeout.seconds(2);

	private IntegerList list;
	
	@Test
	public void noParametersConstructor() {
		list=new IntegerList();
		assertThat(list.toString()).isEqualTo("[]");

	}
	
	@Test
	public void addLastTest(){
		list=new IntegerList();
		list.addLast(1);
		assertThat(list.toString()).isEqualTo("[1]");
		list.addLast(3);
		assertThat(list.toString()).isEqualTo("[1 3]");
		list.addLast(5);
		assertThat(list.toString()).isEqualTo("[1 3 5]");
		list.addLast(7);
		assertThat(list.toString()).isEqualTo("[1 3 5 7]");
	}
	
}
