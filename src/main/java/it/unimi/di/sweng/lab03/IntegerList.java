package it.unimi.di.sweng.lab03;

public class IntegerList {
	
	private IntegerNode head=null;
	private IntegerNode tail=null;

	@Override
	public String toString() {
		StringBuilder result= new StringBuilder("[");
		IntegerNode currentNode = head;
		int i=0;
		while(currentNode!=null){
			if(i++>0)
				result.append(" ");
			 result.append(currentNode.getValue());
			 currentNode=currentNode.next();}
		
		return result.append("]").toString();
	}

	public void addLast(int value) {
		if(head==null){
			head=tail=new IntegerNode(value);
			
		}else{ 
			IntegerNode node=new IntegerNode(value);
			tail.setNext(node);
			tail=node;
		 
		}
		
	}

	
	
}
